;; -*- no-byte-compile: t; -*-
;;; private/my/packages.el

(package!  aweshell :recipe (:fetcher github :repo "manateelazycat/aweshell" :files ("*")))

;; (package! evil-matchit :recipe (:fetcher github :repo "redguardtoo/evil-matchit" :commit "7d65b4167b1f0086c2b42b3aec805e47a0d355c4"))


(package! avy)
(package! lispyville)
(package! pamparam)
(package! company-lsp)
(package! clang-format)

(package! sml-mode)
(package! cmake-mode)
(package! eglot)
(package! lsp-mode)
(package! lsp-ui)
(package! lsp-haskell)
(package! kotlin-mode)
(package! company-lsp)
(package! spinner)
(package! org-brain)

(package! nand2tetris :recipe (:fetcher github :repo "FirstLoveLife/nand2tetris.el" :files ("*")))
(package! flymake-sml :recipe (:fetcher github :repo "oskimura/flymake-sml" :files ("*")))

(package! company-sml :recipe (:fetcher github :repo "nverno/company-sml" :files ("*")))

;(package! rmsbolt
;	  :recipe
;  (:files (:defaults "starters")
;          :fetcher gitlab
;          :repo "jgkamat/rmsbolt"))

;(package! exwm)
(package! sicp)
(package! lsp-rust)

(package! helm-rg)
(package! google-translate)
(package! ccls)
(package! posframe)


(package! org-ref)
(package! adoc-mode)
(package! pyim)
(package! pyim-basedict)

